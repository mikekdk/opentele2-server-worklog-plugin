package org.opentele.server.worklog

import grails.plugin.springsecurity.annotation.Secured
import org.opentele.server.core.model.types.MeasurementFilterType
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.core.model.types.Unit
import org.opentele.server.core.util.MeasurementResult
import org.opentele.server.core.util.TimeFilter
import org.opentele.server.model.Clinician
import org.opentele.server.model.Conference
import org.opentele.server.model.Consultation
import org.opentele.server.model.Measurement
import org.opentele.server.model.Message
import org.opentele.server.model.Patient
import org.opentele.server.model.PatientNote
import org.opentele.server.model.User

import java.text.SimpleDateFormat

@Secured(PermissionName.NONE)
class WorkLogController {

    // --*-- Fields --*--

    private static final NO_VALUE = "NO_VALUE"

    def springSecurityService
    def questionnaireService

    static allowedMethods = [show: 'GET', search:'GET']

    // --*-- Actions --*--

    @Secured(PermissionName.WORKLOG_READ)
    def show() {
        // Empty
    }

    @Secured(PermissionName.WORKLOG_READ)
    def search() {
        String workLogText = createWorkLog(params)
        render(view:'show', model: [workLog:workLogText])
    }

    // --*-- Methods --*--

    private String createWorkLog(Map params) {

        String patientId = params.id
        def patient = Patient.get(patientId)
        def clinician = currentClinician()

        SimpleDateFormat outputDateFormat =
                new SimpleDateFormat(message(code: "workLog.header.dateFormat") as String)
        SimpleDateFormat workLogDateFormat =
                new SimpleDateFormat(message(code: "workLog.entry.dateFormat") as String)

        Date startDate = params.startDate
        Date endDate = params.endDate

        Calendar cal = Calendar.getInstance()
        cal.setTime(endDate)
        cal.set(Calendar.HOUR_OF_DAY, 23)
        cal.set(Calendar.MINUTE, 59)
        cal.set(Calendar.SECOND, 59)
        endDate = cal.getTime()

        if (!endDate.after(startDate)) {
            flash.error = message(code: 'workLog.form.error.invalidInterval')
            redirect(action: "show", id: patient.id)
            return ""
        }

        String resultWorkLogText = ""
        resultWorkLogText = appendHeader(params.title as String, patient,
                resultWorkLogText, startDate, endDate, outputDateFormat)
        resultWorkLogText = appendMeasurements(resultWorkLogText, startDate,
                endDate, patient, workLogDateFormat)
        resultWorkLogText = appendMessages(resultWorkLogText, patient,
                workLogDateFormat, startDate, endDate)
        resultWorkLogText = appendNotes(resultWorkLogText, patient, clinician,
                workLogDateFormat, startDate, endDate)
        resultWorkLogText = appendFooter(resultWorkLogText)

        resultWorkLogText
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private String appendHeader(String title, Patient patient, String resultWorkLogText,
                                Date startDate, Date endDate, outputDateFormat) {

        String titleLabel = message(code: "workLog.header.title")
        String patientLabel = message(code: "workLog.header.patient")
        String startDateLabel = message(code: "workLog.header.startDate")
        String endDateLabel = message(code: "workLog.header.endDate")

        String startDateValue = outputDateFormat.format(startDate)
        String endDateValue = outputDateFormat.format(endDate)

        StringBuffer resultBuffer = new StringBuffer(resultWorkLogText)
        resultBuffer.append("""\
====================
${titleLabel}: ${title}
${patientLabel}: ${patient.getName()}
${startDateLabel}: ${startDateValue}
${endDateLabel}: ${endDateValue}
====================\n\n""")

        resultBuffer.toString()
    }

    private String appendMeasurements(String resultWorkLogText, Date startDate,
                                      Date endDate, Patient patient,
                                      SimpleDateFormat workLogDateFormat) {

        StringBuffer resultBuffer = new StringBuffer(resultWorkLogText)

        TimeFilter timeFilter = TimeFilter.fromFilterType(MeasurementFilterType.CUSTOM, startDate, endDate)

        resultBuffer.append(getConferenceMeasurements(patient, timeFilter, workLogDateFormat))
        resultBuffer.append(getConsultationMeasurements(patient, timeFilter, workLogDateFormat))
        resultBuffer.append(getQuestionnaireMeasurements(patient, timeFilter, workLogDateFormat))

        resultBuffer.toString()
    }

    private getQuestionnaireMeasurements(Patient patient, TimeFilter timeFilter,
                                         workLogDateFormat) {

        def questionnaireResults = questionnaireService.extractCompletedQuestionnaireWithAnswers(
                patient.id, null, false, timeFilter)

        String label = message(code: 'workLog.questionnaireMeasurements.title')
        StringBuffer resultBuffer = new StringBuffer("----- ${label} -----\n\n")

        def measurementTitles = questionnaireResults.questions.collect { it -> it.questionnaireName }
        measurementTitles = measurementTitles.unique(false)
        def measurementTextMap = measurementTitles.collectEntries {
            [(it): "${it}\n"]
        }

        Map measurementDescriptionMap = [:]
        questionnaireResults.questions.eachWithIndex { it, index ->
            measurementDescriptionMap[it.templateQuestionnaireNodeId] = [it, index]
        }

        Set<MeasurementResult> measurementResults = questionnaireResults.results.values().flatten()
        Map<String, Set> measurementResultMap = [:]
        measurementResults.each { MeasurementResult result ->
            Set measurementSet = measurementResultMap.get(result.cqId) ?
                    measurementResultMap.get(result.cqId) :
                    new HashSet()
            measurementSet += result
            measurementResultMap[result.cqId] = measurementSet
        }

        Map columnHeaderMap = questionnaireResults.columnHeaders.collectEntries {
            [(it.id): it]
        }

        def columnHeaderDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

        if (measurementResultMap.values().isEmpty()) {
            resultBuffer.append(message(code: "workLog.noMeasurementsInInterval"))
            resultBuffer.append('\n\n')
        }

        measurementResultMap.values().each { Set set ->

            def sortedSet = set.toList().sort { measurementResult ->
                Long tqnId = measurementResult.tqnId
                def (_, idx) = measurementDescriptionMap[tqnId]
                idx
            }

            for (int i = 0; i < sortedSet.size(); i++) {
                def measurementResult = sortedSet[i]
                Long cqId = measurementResult.cqId
                Long tqnId = measurementResult.tqnId
                def (measurementDescription, _) = measurementDescriptionMap[tqnId]
                String keyText = measurementDescription.questionnaireName
                String dateText = columnHeaderMap[cqId].uploadDate
                dateText = workLogDateFormat.format(columnHeaderDateFormat.parse(dateText))
                String questionText = measurementDescription.text

                def nextMeasurementResult = sortedSet[i+1]
                String measurementText
                if (nextMeasurementResult &&
                        nextMeasurementResult.cqId == cqId &&
                        nextMeasurementResult.tqnId == tqnId) {

                    String firstValueText = translateValueText(measurementResult.value as String, measurementResult.unit)
                    String firstUnitText = translateUnitText(measurementResult)
                    String secondValueText = translateValueText(nextMeasurementResult.value as String, nextMeasurementResult.unit)
                    String secondUnitText = translateUnitText(nextMeasurementResult)
                    if (firstUnitText < secondUnitText) {
                        measurementText = createExtendedMeasurementText(dateText, questionText,
                                firstValueText, firstUnitText, secondValueText, secondUnitText)
                    } else {
                        measurementText = createExtendedMeasurementText(dateText, questionText,
                                secondValueText, secondUnitText, firstValueText, firstUnitText)
                    }
                    i++
                } else {
                    String valueText = translateValueText(measurementResult.value as String, measurementResult.unit)
                    String unitText = translateUnitText(measurementResult)
                    measurementText = createMeasurementText(dateText, questionText, valueText, unitText)
                }

                measurementTextMap[keyText] += measurementText
            }
        }

        measurementTextMap.values().each { it -> resultBuffer.append("${it}\n") }

        resultBuffer.toString()
    }

    private getConferenceMeasurements(Patient patient, TimeFilter timeFilter, workLogDateFormat) {
        def conferences = Conference.findAllByPatientAndCompletedAndCreatedDateBetween(
                patient, true, timeFilter.start, timeFilter.end)
        Map<String, List<Measurement>> typeNameToMeasurementMap = extractTypeToMeasurementMap(conferences)
        String label = message(code: 'workLog.conferenceMeasurements.title')
        generateResultTextFromMeasurementMap(label, typeNameToMeasurementMap, workLogDateFormat)
    }

    private getConsultationMeasurements(Patient patient, TimeFilter timeFilter, workLogDateFormat) {
        def consultations = Consultation.findAllByPatientAndCreatedDateBetween(
                patient, timeFilter.start, timeFilter.end)
        Map<String, List<Measurement>> typeNameToMeasurementMap = extractTypeToMeasurementMap(consultations)
        String label = message(code: 'workLog.consultationMeasurements.title')
        generateResultTextFromMeasurementMap(label, typeNameToMeasurementMap, workLogDateFormat)
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private Map<String, List<Measurement>> extractTypeToMeasurementMap(List collection) {
        List<Measurement> measurements = (collection.collect { object ->
            object.measurements
        }).flatten() as List<Measurement>
        Map<String, List<Measurement>> typeNameToMeasurementMap = measurements.groupBy { measurement ->
            measurement.measurementType.name.name()
        }
        typeNameToMeasurementMap
    }

    private generateResultTextFromMeasurementMap(String label,
                                                 Map<String, List<Measurement>> typeNameToMeasurementMap,
                                                 workLogDateFormat) {

        StringBuffer resultBuffer = new StringBuffer("----- ${label} -----\n\n")
        if (typeNameToMeasurementMap.isEmpty()) {
            resultBuffer.append(message(code: "workLog.noMeasurementsInInterval"))
            resultBuffer.append('\n\n')
        }

        typeNameToMeasurementMap.each { measurementType, measurementList ->
            resultBuffer.append(message(code: "enum.measurementType.${measurementType}") + '\n')
            measurementList.each { measurement ->
                String dateText = workLogDateFormat.format(measurement.modifiedDate)
                String valueText = translateValueText(measurement.toString(), null)
                String unitText = translateUnitText(measurement)
                String measurementText = createMeasurementText(dateText, null, valueText, unitText)
                resultBuffer.append(measurementText)
            }
            resultBuffer.append('\n')
        }

        resultBuffer.toString()
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private String appendMessages(String resultWorkLogText, Patient patient,
                                  workLogDateFormat, Date startDate, Date endDate) {

        String messagesLabel = message(code: "workLog.header.messages")
        StringBuffer resultBuffer = new StringBuffer(resultWorkLogText)
        resultBuffer.append("-------- ${messagesLabel} --------\n\n")

        List<Message> messageList = extractMessages(patient, startDate, endDate)

        if (messageList.isEmpty()) {
            resultBuffer.append(message(code: "workLog.noMessagesInInterval"))
            resultBuffer.append('\n')
        }

        messageList.each { it ->
            def dateText = workLogDateFormat.format(it.sendDate)
            def recipientText = it.sentByPatient ?
                    "${patient.name} -> ${it.department}" :
                    "${it.department} -> ${patient.name}"
            def messageText = "\"${it.title}\": \"${it.text}\""
            resultBuffer.append("${dateText} : ${recipientText}, ${messageText}\n")
        }

        resultBuffer.toString()
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private List<Message> extractMessages(Patient patient, Date startDate, Date endDate) {
        def messages = Message.findAll("FROM Message AS m " +
                "WHERE m.patient = (:patient) " +
                "AND (:startDate) <= m.modifiedDate " +
                "AND m.modifiedDate <= (:endDate) " +
                "ORDER BY m.sendDate desc",
                [patient: patient, startDate: startDate, endDate: endDate])
        messages
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private String appendNotes(String resultWorkLogText, Patient patient,
                               Clinician clinician, workLogDateFormat,
                               Date startDate, Date endDate) {

        String notesLabel = message(code: "workLog.header.notes")
        StringBuffer resultBuffer = new StringBuffer(resultWorkLogText)
        resultBuffer.append("\n-------- ${notesLabel} --------\n\n")

        List<PatientNote> notes = extractNotes(patient, startDate, endDate)

        if (notes.isEmpty()) {
            resultBuffer.append(message(code: "workLog.noNotesInInterval"))
            resultBuffer.append('\n')
        }

        notes.each { it ->
            def dateText = workLogDateFormat.format(it.createdDate)
            def nameText = "${clinician.firstName} ${clinician.lastName}"
            def noteText = "\"${it.note}\""
            resultBuffer.append("${dateText} : ${nameText}, ${noteText}\n")
        }

        resultBuffer.toString()
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private List<PatientNote> extractNotes(Patient patient, Date startDate, Date endDate) {
        def notes = PatientNote.findAll("FROM PatientNote AS pn " +
                "WHERE pn.patient = (:patient) " +
                "AND (:startDate) <= pn.modifiedDate " +
                "AND pn.modifiedDate <= (:endDate) " +
                "ORDER BY pn.modifiedDate DESC",
                [patient: patient, startDate: startDate, endDate: endDate])
        notes
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private String appendFooter(String resultWorkLogText) {
        resultWorkLogText += "\n===================="
        resultWorkLogText
    }

    private String translateValueText(String value, Unit unit) {
        String valueText = (value == NO_VALUE) ? message(code: "workLog.measurements.noValue") : value
        valueText = message(code: "default.yesno.${valueText}", default: valueText)

        if (unit == Unit.BPM || unit == Unit.MGRAM_L) {
            valueText = valueText.substring(0, valueText.indexOf('.'))
        }
        if (unit == Unit.MMHG) {
            def (systolic , diastolic) = valueText.split(',')
            systolic = systolic.substring(0, systolic.indexOf('.'))
            diastolic = diastolic.substring(0, diastolic.indexOf('.'))
            valueText = "${systolic}/${diastolic}"
        }

        valueText
    }

    private String translateUnitText(measurement) {
        String unitText = measurement.unit ? measurement.unit.value() : ""
        unitText = message(code: "enum.unit.${unitText.toUpperCase()}", default: unitText)
        unitText
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private String createMeasurementText(String dateText, String questionText,
                                         String valueText, String unitText) {
        String measurementText
        String questionTextModified = questionText ? " ${questionText}," : ""
        measurementText = "${dateText} :${questionTextModified} ${valueText} ${unitText}\n"
        measurementText
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private String createExtendedMeasurementText(String dateText, String questionText,
                                                 String firstValueText, String firstUnitText,
                                                 String secondValueText, String secondUnitText) {
        String measurementText
        String questionTextModified = questionText ? " ${questionText}," : ""
        measurementText = "${dateText} :${questionTextModified} ${firstValueText} " +
                "${firstUnitText}, ${secondValueText} ${secondUnitText} \n"
        measurementText
    }

    private currentClinician() {
        Clinician.findByUser(springSecurityService.currentUser as User)
    }

}