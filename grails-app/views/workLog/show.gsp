<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout"
          content="main">
    <title>
        <g:message code="workLog.show.title"/>
    </title>
    <script>
        $(document).ready(function() {
            $('#copyButton').click(function(event) {
                event.preventDefault();
                var popupMessage = "${message(code: 'workLog.copyToClipboard')}";
                window.prompt(popupMessage, $('#resultText').val());
            });
        });
    </script>
</head>
<body>

<div id="work-log-content"
     class="content"
     role="main">

    <h1><g:message code="workLog.show.title"/></h1>

    <form>
        <fieldset class="form">

            <g:if test="${flash.error}">
                <div class="errors" role="status">
                    ${flash.error}
                </div>
            </g:if>

            <div class="fieldcontain required">
                <label for="workLogTitle"
                       data-tooltip="${message(code: 'workLog.form.tooltip.title')}">
                    <g:message code="workLog.form.title"/>
                    <span class="required-indicator">*</span>
                </label>
                <g:textField id="workLogTitle"
                             name="title"
                             data-tooltip="${message(code: 'workLog.form.tooltip.title')}"
                             value="${title}"/><br/>
            </div>

            <div class="fieldcontain required">
                <label for="startDate"
                       data-tooltip="${message(code: 'workLog.form.tooltip.startDate')}">
                    <g:message code="workLog.form.startDate"/>
                    <span class="required-indicator">*</span>
                </label>
                <span id="startDate"
                      data-tooltip="${message(code: 'workLog.form.tooltip.startDate')}">
                    <jq:datePicker default="none"
                                   name="startDate"
                                   precision="day"
                                   years="${2013..2050}"/>
                </span>
            </div>

            <div class="fieldcontain required">
                <label for="endDate"
                       data-tooltip="${message(code: 'workLog.form.tooltip.endDate')}">
                    <g:message code="workLog.form.endDate"/>
                    <span class="required-indicator">*</span>
                </label>
                <span id="endDate"
                      data-tooltip="${message(code: 'workLog.form.tooltip.endDate')}">
                    <jq:datePicker default="none"
                                   name="endDate"
                                   precision="day"
                                   years="${2013..2050}"/>
                </span>
            </div>

            <div class="buttons">
                <g:actionSubmit action="search"
                                class="search"
                                method="get"
                                value="${message(code: "workLog.form.submit")}"/>
            </div>
        </fieldset>
    </form>

    <div>
        <g:textArea id="resultText"
                    name="resultText"
                    value="${workLog}"
                    style="height: 400px; width: 800px"/>
    </div>

    <form>
        <fieldset class="buttons">
            <g:actionSubmit id="copyButton"
                            class="gonext"
                            value="${message(code: 'workLog.form.copy')}"/>
        </fieldset>
    </form>

</div>
</body>
</html>
